<?php

//////////////////////////////////////////////////////////////////////////////
// Core API hooks

/**
 * Implementation of hook_menu().
 */
function digitalcurrency_menu() {
  return array(
    // Administer >> Site configuration >> Digital currency
    'admin/settings/digitalcurrency' => array(
      'title' => 'Digital currency',
      'description' => 'Settings for digital currency modules.',
      'access arguments' => array('administer site configuration'),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('digitalcurrency_admin_settings'),
      'file' => 'digitalcurrency.admin.inc',
    ),
  );
}

/**
 * Implementation of hook_block().
 */
function digitalcurrency_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {
    case 'list':
      return array(
        'buttons' => array(
          'info'   => t('Digital currency buttons'),
          'region' => 'right',
          'weight' => 10,
          'cache'  => BLOCK_CACHE_GLOBAL,
        ),
      );

    case 'configure':
      switch ($delta) {
        case 'buttons':
          module_load_include('inc', 'digitalcurrency', 'digitalcurrency.admin');
          return digitalcurrency_admin_block_configure();
      }
      break;

    case 'save':
      switch ($delta) {
        case 'buttons':
          module_load_include('inc', 'digitalcurrency', 'digitalcurrency.admin');
          return digitalcurrency_admin_block_submit($edit);
      }
      break;

    case 'view':
      $block = array();
      switch ($delta) {
        case 'buttons':
          $output = array();
          foreach (digitalcurrency_get_modules() as $module => $title) {
            if (variable_get('digitalcurrency_' . $module . '_button', 1)) {
              $output[] = '<div class="digitalcurrency-button ' . $module . '">';
              $output[] = l(theme('digitalcurrency_button', $module, $title), digitalcurrency_get_link($module), array('html' => TRUE));
              $output[] = '</div>';
            }
          }
          $block['content'] = implode('', $output);
          break;
      }
      return $block;
  }
}

/**
 * Implementation of hook_cron().
 */
function digitalcurrency_cron() {
  // TODO: purge old exchange rates.
}

//////////////////////////////////////////////////////////////////////////////
// User API hooks

/**
 * Implementation of hook_user().
 */
function digitalcurrency_user($op, &$edit, &$account, $category = NULL) {
  switch ($op) {
    // TODO
  }
}

//////////////////////////////////////////////////////////////////////////////
// Theme API hooks

function digitalcurrency_theme() {
  return array(
    'digitalcurrency_button' => array(
      'arguments' => array('gateway' => NULL, 'title' => NULL),
    ),
    'digitalcurrency_cart_details_form' => array(
      'arguments' => array('form' => NULL),
    ),
    'digitalcurrency_admin_settings' => array(
      'arguments' => array('form' => NULL),
      'file'      => 'digitalcurrency.admin.inc',
    ),
  );
}

//////////////////////////////////////////////////////////////////////////////
// Agora API hooks

/**
 * Implementation of hook_agora_accounts().
 */ 
function digitalcurrency_agora_accounts() {
  return array(
    'digitalcurrency' => array(
      'title'  => t('Digital currency account'),
      'type'   => TRUE,
      'types'  => array( // FIXME
        'cgold'          => t('c-gold'),
        'ecache'         => t('eCache'),
        'ecumoney'       => t('ECUmoney'),
        'igolder'        => t('iGolder'),
        'libertyreserve' => t('Liberty Reserve'),
        'loom'           => t('Loom'),
        'pecunix'        => t('Pecunix'),
        'perfectmoney'   => t('Perfect Money'),
        'trubanc'        => t('Trubanc'),
        'vmoney'         => t('V-Money'),
        'webmoney'       => t('WebMoney'),
      ),
      'number' => TRUE,
    ),
  );
}

//////////////////////////////////////////////////////////////////////////////
// Ubertcart API hooks

function digitalcurrency_payment_method() {
  return array(
    'digitalcurrency' => array(
      'id'         => 'digitalcurrency',
      'name'       => t('Digital currency'),
      'title'      => t('Digital currency'),
      'desc'       => t('Pay using digital currencies.'),
      'callback'   => 'digitalcurrency_payment_method_callback',
      'weight'     => 1,
      'checkout'   => TRUE,
    ),
  );
}

function digitalcurrency_payment_method_callback($op, &$arg1) {
  switch ($op) {
    case 'cart-details':
      return uc_strip_form(drupal_get_form('digitalcurrency_cart_details_form', $arg1));

    case 'cart-process':
      $_SESSION['digitalcurrency'] = $_POST['digitalcurrency'];
      return;

    case 'settings':
      $form = array();
      // TODO
      return $form;
  }
}

function digitalcurrency_cart_details_form($form_state, $order) {
  $form = $options = array();
  foreach (digitalcurrency_get_gateways() as $module => $title) {
    $options[$module] = theme('digitalcurrency_button', $module, $title);
  }
  $form['digitalcurrency'] = array(
    '#type'          => 'radios',
    '#title'         => '',
    '#options'       => $options,
    '#default_value' => variable_get('uc_payment_digitalcurrency_gateway', ''),
  );
  return $form;
}

function theme_digitalcurrency_button($module, $title, array $options = array()) {
  $button = ($info = module_invoke($module, 'digitalcurrency')) ?
    drupal_get_path('module', $module) . '/' . $info['button'] : NULL;
  $options = array_merge(array('width' => 88, 'height' => 31, 'style' => 'position: relative; top: 10px;'), $options);
  return empty($button) ? $title : theme('image', $button, $title, t('Pay with @title', array('@title' => $title)), $options, FALSE);
}

function theme_digitalcurrency_cart_details_form($form) {
  return drupal_render($form); // TODO
}

function digitalcurrency_form_alter(&$form, &$form_state, $form_id) {
  switch ($form_id) {
    // Review order form
    case 'uc_cart_checkout_review_form':
      if (($order_id = intval($_SESSION['cart_order'])) > 0) {
        if (($order = uc_order_load($order_id)) && $order->payment_method == 'digitalcurrency') {
          // Override the default submit button
          unset($form['submit']);
          $form['#prefix'] = '<table style="display: inline; padding-top: 1em;"><tr><td>';
          $form['#suffix'] = '</td><td>' . drupal_get_form('digitalcurrency_submit_order_form', $order) . '</td></tr></table>';
        }
      }
      break;
  }
}

function digitalcurrency_submit_order_form($form_state, $order) {
  $gateways = _payment_gateway_list('digitalcurrency', FALSE);
  $gateway  = $_SESSION['digitalcurrency'];

  if (isset($gateways[$gateway]['file'])) {
    module_load_include('inc', $gateway, basename($gateways[$gateway]['file'], '.inc'));
  }

  $form = array('#action' => $gateways[$gateway]['submit_action']);
  foreach (call_user_func($gateways[$gateway]['submit_data'], $order) as $field => $value) {
    $form[$field] = array('#type' => 'hidden', '#value' => $value);
  }
  $form['submit'] = array('#type' => 'submit', '#value' => variable_get('uc_checkout_submit_button', t('Submit order')));
  return $form;
}

//////////////////////////////////////////////////////////////////////////////
// Digital Currency API implementation

function digitalcurrency_get_modules() {
  $modules = array();
  foreach (module_implements('digitalcurrency') as $module) { // FIXME
    $module_info = unserialize(db_result(db_query("SELECT info FROM {system} WHERE type = 'module' AND name = '%s'", $module)));
    $modules[$module] = $module_info ? $module_info['name'] : $module;
  }
  return $modules;
}

function digitalcurrency_get_gateways() {
  $gateways = array();
  foreach (_payment_gateway_list('digitalcurrency', TRUE) as $id => $gateway) {
    $gateways[$id] = $gateway['title'];
  }
  return $gateways;
}

function digitalcurrency_get_link($module) {
  return variable_get('digitalcurrency_' . $module . '_link', @constant(strtoupper($module . '_url')));
}

function digitalcurrency_get_rate($unit1, $unit2, $source = NULL, $timestamp = NULL) {
  $rates = digitalcurrency_get_rates($unit1, $unit2, $source, $timestamp);
  return isset($rates[$unit1][$unit2]) ? $rates[$unit1][$unit2] : NULL;
}

function digitalcurrency_get_rates($unit1 = NULL, $unit2 = NULL, $source = NULL, $timestamp = NULL) {
  $filter = array();
  foreach (array('unit_from', 'unit_to', 'source', 'timestamp') as $arg => $field) {
    if ($arg < func_num_args() && (($value = func_get_arg($arg)) != NULL)) {
      $filter["($field = '%s')"] = $value;
    }
  }

  $query  = 'SELECT * FROM {digitalcurrency_rates}' .
    ($filter ? ' WHERE ' . implode(' AND ', array_keys($filter)) : '') . ' ORDER BY unit_from, unit_to';
  $result = db_query($query, array_values($filter));
  $rates  = array();
  while ($record = db_fetch_object($result)) {
    $rates[$record->unit_from][$record->unit_to] = $record->rate; // FIXME
  }
  return $rates;
}

function digitalcurrency_set_rate($unit1, $unit2, $rate, $source = NULL, $timestamp = NULL) {
  drupal_write_record('digitalcurrency_rates', $record = (object)array(
    'unit_from' => $unit1,
    'unit_to'   => $unit2,
    'rate'      => is_string($rate) ? $rate : sprintf('%.4f', $rate),
    'source'    => $source,
    'timestamp' => $timestamp,
  ));
}
