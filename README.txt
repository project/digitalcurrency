
Digital currencies for Drupal
=============================
This is an ensemble of modules that provide Ubercart payment integration for
the following digital currencies:

  * Pecunix
    <http://pecunix.com/>

  * Liberty Reserve
    <http://www.libertyreserve.com/>

  * Perfect Money
    <https://perfectmoney.com/>


BUG REPORTS
-----------
Post bug reports and feature requests to the issue tracking system at:

  <http://drupal.org/node/add/project-issue/digitalcurrency>


CREDITS
-------
Developed and maintained by Arto Bendiken <http://bendiken.net/>
