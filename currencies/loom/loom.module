<?php

//////////////////////////////////////////////////////////////////////////////
// Module settings

define('LOOM_URL', 'https://loom.cc/');

//////////////////////////////////////////////////////////////////////////////
// Core API hooks

/**
 * Implementation of hook_help().
 */
function loom_help($path) {
  switch ($path) {
    case 'admin/settings/digitalcurrency/loom':
      return '<p>' . theme('digitalcurrency_button', 'loom', t('Loom'), array('align' => 'right', 'style' => 'margin: 0px 0px 10px 10px;')) .
        t('<a href="@link" target="_blank">Loom</a> is an anonymous digital trading and value transfer system. Payments are irrevocable and private, and can be denominated in a wide variety of asset types and currencies issued by Loom users themselves. Loom provides programmatic (API) account access.', array('@link' => LOOM_URL)) . '</p>';
  }
}

/**
 * Implementation of hook_menu().
 */
function loom_menu() {
  return array(
    // Administer >> Site configuration >> Digital currency >> Loom
    'admin/settings/digitalcurrency/loom' => array(
      'title' => 'Loom',
      'description' => 'Settings for the Loom digital currency module.',
      'access arguments' => array('administer site configuration'),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('loom_admin_settings'),
      'file' => 'loom.admin.inc',
    ),
    'admin/settings/digitalcurrency/loom/delete/%' => array(
      'title' => 'Delete asset type',
      'type' => MENU_CALLBACK,
      'access arguments' => array('administer site configuration'),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('loom_admin_currency_delete', 5),
      'file' => 'loom.admin.inc',
    ),
  );
}

/**
 * Implementation of hook_theme()
 */
function loom_theme() {
  return array(
    'loom_admin_settings' => array(
      'arguments' => array('form' => NULL),
      'file' => 'loom.admin.inc',
    ),
  );
}

/**
 * Implementation of hook_init().
 */
function loom_init() {
  @module_load_include('php', 'loom', 'vendor/loomclient/LoomClient');
}

//////////////////////////////////////////////////////////////////////////////
// Digital Currency API hooks

/**
 * Implementation of hook_digitalcurrency().
 */
function loom_digitalcurrency() {
  return array('api' => TRUE, 'sci' => FALSE, 'button' => 'loom.gif');
}

//////////////////////////////////////////////////////////////////////////////
// Miscellaneous

function loom_get_currency($id) {
  $currencies = loom_get_currencies();
  return isset($currencies[$id]) ? $currencies[$id] : NULL;
}

function loom_get_currencies($op = NULL) {
  $result = db_query("SELECT * FROM {loom_currencies} ORDER BY name ASC");
  $currencies = array();
  while ($row = db_fetch_object($result)) {
    $currencies[$row->id] = $row;
  }
  switch ($op) {
    case 'enabled':
      foreach ($currencies as $id => $currency) {
        if (empty($currency->enabled)) {
          unset($currencies[$id]);
        }
      }
      break;
  }
  return $currencies;
}

function loom_parse_currency($spec) {
  if (preg_match('/^id:\s+([0-9a-f]{32})\s+scale:\s+(\d+)\s+precision:\s+(\d+)\s+name:\s+(.*)$$/', trim($spec), $matches)) {
    return (object)array(
      'id'        => $matches[1],
      'name'      => trim($matches[4]),
      'scale'     => (int)$matches[2],
      'precision' => (int)$matches[3],
    );
  }
}

function loom_insert_currency($id, $currency) {
  $row = (object)array_merge(array('id' => $id, 'name' => $id, 'scale' => 0, 'precision' => 0, 'unit' => NULL, 'enabled' => FALSE, 'weight' => 0, 'link' => NULL), (array)$currency);
  return db_query("INSERT INTO {loom_currencies} VALUES ('%s', '%s', %d, %d, '%s', %d, %d, '%s')", $row->id, $row->name, $row->scale, $row->precision, $row->unit, $row->enabled, $row->weight, $row->link);
}

function loom_delete_currency($id) {
  return db_query("DELETE FROM {loom_currencies} WHERE id = '%s'", $id);
}
