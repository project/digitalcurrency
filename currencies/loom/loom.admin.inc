<?php

//////////////////////////////////////////////////////////////////////////////
// Module settings

function loom_admin_settings() {
  module_load_include('inc', 'loom', 'loom.api');
  $form = array();

  // Asset types
  $form['currencies'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Asset types'),
    '#collapsible'   => TRUE,
    '#collapsed'     => FALSE,
    '#description'   => loom_help('admin/settings/digitalcurrency/loom#currencies'),
  );
  $currencies = loom_get_currencies();
  $form['currencies']['loom_currencies'] = array(
    '#type'          => 'checkboxes',
    '#title'         => '',
    '#options'       => array_combine(array_keys($currencies), array_fill(0, count($currencies), '')),
    '#default_value' => array_keys(loom_get_currencies('enabled')),
    '#description'   => t('If you wish to import one of those one-line pieces of text which describes the entire asset type all at once, copy and paste it into the text field below and press the Import button.'), // TODO
  );
  $form['currencies']['loom_currency'] = array(
    '#type'          => 'textfield',
    '#maxlength'     => 255,
    '#attributes'    => array('style' => 'width: 95%;'),
  );
  $form['currencies']['loom_currency_import'] = array('#type' => 'submit', '#value' => t('Import'));

  // Payment settings
  loom_payment_settings($form, FALSE);

  $form['#submit'][] = 'loom_admin_settings_submit';
  return array_merge_recursive(system_settings_form($form), array('#theme' => 'loom_admin_settings', 'buttons' => array('#weight' => 99)));
}

function loom_admin_settings_validate($form, &$form_state) {
  extract($form_state['values'], EXTR_SKIP | EXTR_REFS);

  // Validate a Loom asset type description, if one was given:
  $loom_currency = trim($loom_currency);
  if (!empty($loom_currency)) {
    if (!($currency = loom_parse_currency($loom_currency))) {
      form_set_error('loom_currency', t('The asset type description line is invalid: %text', array('%text' => $loom_currency)));
    }
    else if (($currency = loom_get_currency($currency->id))) {
      form_set_error('loom_currency', t('The asset type %id (%name) is already defined. To replace an asset type definition, delete the existing entry first.', array('%id' => $currency->id, '%name' => $currency->name)));
    }
  }
}

function loom_admin_settings_submit($form, &$form_state) {
  extract($form_state['values'], EXTR_SKIP | EXTR_REFS);

  // Update the enabled status for all Loom asset types:
  foreach ($loom_currencies as $id => $enabled) {
    db_query("UPDATE {loom_currencies} SET enabled = %d WHERE id = '%s'", !empty($enabled), $id);
  }

  // Import a new Loom asset type description, if one was given:
  if (($currency = loom_parse_currency($loom_currency))) {
    loom_insert_currency($currency->id, $currency);
    drupal_set_message(t('The asset type %id (%name) has been imported.', array('%id' => $currency->id, '%name' => $currency->name)));
  }

  // Make sure we don't store unnecessary junk in the {variable} table:
  foreach (array('currencies', 'currency', 'currency_import') as $field) {
    unset($form_state['values']['loom_' . $field]);
  }
}

function theme_loom_admin_settings($form) {
  $head = array(t('Enabled'), array('data' => t('Name'), 'width' => '100%'), t('Scale'), t('Precision'), t('Denomination'), array('data' => t('Operations'), 'colspan' => '2'));

  $rows = array();
  foreach (loom_get_currencies() as $id => $currency) {
    $rows[] = array(
      array('data' => drupal_render($form['currencies']['loom_currencies'][$id]), 'align' => 'center'),
      empty($currency->link) ? $currency->name : l($currency->name, $currency->link, array('attributes' => array('target' => '_blank'))),
      $currency->scale,
      $currency->precision,
      $currency->unit, // TODO: drop-down selection box
      l(t('delete'), 'admin/settings/digitalcurrency/loom/delete/' . $id),
      '',              // TODO: copy to clipboard
    );
  }

  $rows[] = array('',
    array('data' => drupal_render($form['currencies']['loom_currency']), 'colspan' => 4),
    array('data' => drupal_render($form['currencies']['loom_currency_import']), 'colspan' => 2),
  );

  $form['currencies']['#value'] = theme('table', $head, $rows, array('class' => 'loom currencies'));
  return drupal_render($form);
}

//////////////////////////////////////////////////////////////////////////////
// Payment settings

function loom_payment_settings(&$form = array(), $extended = TRUE) {
  loom_payment_settings_api($form);
  return $form;
}

function loom_payment_settings_api(&$form = array()) {
  // API credentials
  $form['loom_api'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('API credentials'),
    '#description'   => t('Settings for the Loom <a href="https://loom.cc/?function=help&topic=developers" target="_blank">Application Programming Interface (API)</a>.'),
    '#collapsible'   => TRUE,
    '#collapsed'     => TRUE,
  );
  // TODO
  return $form;
}

//////////////////////////////////////////////////////////////////////////////
// Asset type deletion

function loom_admin_currency_delete($form_state, $id) {
  $currency = loom_get_currency($id);

  return confirm_form($form = array('id' => array('#type' => 'value', '#value' => $id)),
    t('Are you sure you want to delete the asset type %name?', array('%name' => $currency->name)),
    isset($_GET['destination']) ? $_GET['destination'] : 'admin/settings/digitalcurrency/loom',
    t('This action cannot be undone.'));
}

function loom_admin_currency_delete_submit($form, &$form_state) {
  $currency = loom_get_currency($id = $form_state['values']['id']);

  if ($form_state['values']['confirm']) {
    loom_delete_currency($id);
    drupal_set_message(t('The asset type %id (%name) has been deleted.', array('%id' => $currency->id, '%name' => $currency->name)));
    $form_state['redirect'] = 'admin/settings/digitalcurrency/loom';
  }
}
