<?php

//////////////////////////////////////////////////////////////////////////////
// Perfect Money SCI settings

define('PERFECTMONEY_SCI_URL', 'https://perfectmoney.com/api/step1.asp');

//////////////////////////////////////////////////////////////////////////////
// Ubertcart API hooks

function perfectmoney_payment_data($order) {
  //$order->order_total = 0.01; // DEBUG

  // TODO: we should support automatic currency exchange calculations here.
  $unit = variable_get('perfectmoney_payment_unit', variable_get('uc_currency_code', 'USD'));

  return array(
    // Fields required by Perfect Money
    'PAYEE_ACCOUNT'  => variable_get('perfectmoney_sci_payee_account_' . strtolower($unit), ''),
    'PAYEE_NAME'     => variable_get('perfectmoney_sci_payee_name', variable_get('uc_store_name', variable_get('site_name', 'Drupal'))),
    'PAYMENT_UNITS'  => $unit,
    'PAYMENT_AMOUNT' => uc_currency_format($order->order_total, FALSE, FALSE, '.'),
    'PAYMENT_URL'    => url('cart/perfectmoney/finish/' . uc_cart_get_id(), array('absolute' => TRUE)),
    'NOPAYMENT_URL'  => url('cart/perfectmoney/cancel/' . uc_cart_get_id(), array('absolute' => TRUE)),
    // Optional fields
    'STATUS_URL'     => url('cart/perfectmoney/status/' . uc_cart_get_id(), array('absolute' => TRUE)),
    'PAYMENT_ID'     => $order->order_id,
    'SUGGESTED_MEMO' => '',
  );
}

function perfectmoney_payment_charge($order_id, $amount, $data) {
  $order = uc_order_load($order_id); // TODO
}

function perfectmoney_payment_callback($op, $cart_id = NULL) {
  if (!($order = uc_order_load($order_id = $_POST['PAYMENT_ID']))) {
    return drupal_not_found();
  }

  switch ($op) {
    case 'status':
    //case 'finish': // DEBUG
      watchdog('digitalcurrency', 'Receiving payment notification for order #@order.', array('@order' => $order_id), WATCHDOG_NOTICE, l(t('view'), 'admin/store/orders/' . $order->order_id . '/payments'));

      $comment = t('Perfect Money @unit payment, batch number #@id.', array('@unit' => $_POST['PAYMENT_UNITS'], '@id' => $_POST['PAYMENT_BATCH_NUM']));
      uc_payment_enter($order->order_id, 'digitalcurrency', $_POST['PAYMENT_AMOUNT'], 0, $_POST, $comment);

      if (strlen(trim($_POST['SUGGESTED_MEMO'])) > 0) {
        uc_order_comment_save($order->order_id, 0, t('Perfect Money payment note: @text', array('@text' => trim($_POST['SUGGESTED_MEMO']))), 'admin');
      }

      break; // !DEBUG

    case 'finish':
      $output = uc_cart_complete_sale($order, variable_get('uc_new_customer_login', FALSE));
      return ($path = variable_get('uc_cart_checkout_complete_page', '')) ? drupal_goto($path) : $output;

    default:
    case 'cancel':
      uc_order_comment_save($order->order_id, 0, t('Perfect Money payment canceled by customer.'), 'admin');
      return drupal_goto('cart/checkout'); // return to the checkout screen
  }
}
