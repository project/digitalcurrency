<?php

//////////////////////////////////////////////////////////////////////////////
// Perfect Money API constants

define('PERFECTMONEY_API_URL',   'https://perfectmoney.com/acct/');
define('PERFECTMONEY_RATES_URL', PERFECTMONEY_API_URL . 'rates.asp');

//////////////////////////////////////////////////////////////////////////////
// Perfect Money API

/**
 * Returns an array containing the currency codes supported by Perfect Money.
 */
function perfectmoney_get_units() {
  return array('USD', 'EUR', 'XAU');
}

/**
 * Queries current Perfect Money exchange rates (USD, EUR, XAU).
 */
function perfectmoney_get_rates($currency = 'USD') {
  // If no exchange rates have been cached yet, do that now:
  if (!($rates = digitalcurrency_get_rates($currency, NULL, PERFECTMONEY_RATES_URL))) {
    perfectmoney_fetch_and_store_rates();
    $rates = digitalcurrency_get_rates($currency, NULL, PERFECTMONEY_RATES_URL);
  }
  return $rates;
}

/**
 * Fetches current Perfect Money exchange rates and stores them in the
 * exchange rates database provided by the Digital Currency API.
 */
function perfectmoney_fetch_and_store_rates() {
  foreach (perfectmoney_get_units() as $unit1) {
    foreach (perfectmoney_fetch_rates($unit1) as $timestamp => $rates) {
      foreach ($rates as $unit2 => $rate) {
        digitalcurrency_set_rate($unit1, $unit2, $rate, PERFECTMONEY_RATES_URL, $timestamp);
      }
    }
  }
}

/**
 * Fetches current Perfect Money exchange rates (USD, EUR, XAU).
 *
 * @see https://perfectmoney.com/documents/perfectmoney-api.doc#2.5
 */
function perfectmoney_fetch_rates($currency = 'USD') {
  $cur = ($currency == 'XAU' || $currency == 'GAU') ? 'GOLD' : $currency; // normalize XAU
  $response = perfectmoney_query('rates', 'GET', array('CUR' => $cur));
  $rates = array_map('trim', explode(',', $response)); // parse CSV

  // Convert the "%m/%d/%y %I:%M:%S %p" date/time into a Unix timestamp
  $timestamp = strtotime(array_shift($rates));

  // Associate the returned exchange rates with their currency codes
  $currencies = perfectmoney_get_units();
  unset($currencies[array_search($currency, $currencies, TRUE)]);
  $currencies = array_values($currencies);

  return array($timestamp => array($currencies[0] => $rates[0], $currencies[1] => $rates[1]));
}

/**
 * Queries a member's Perfect Money account balances.
 *
 * @see https://perfectmoney.com/documents/perfectmoney-api.doc#2.4
 */
function perfectmoney_get_balances($account_id, $password) {
  return perfectmoney_parse_fields(perfectmoney_query('balance', 'GET', array(
    'AccountID'  => $account_id,
    'PassPhrase' => $password,
  )));
}

/**
 * Queries a member's Perfect Money account history.
 *
 * @see https://perfectmoney.com/documents/perfectmoney-api.doc#2.3
 */
function perfectmoney_get_history($account_id, $password, array $options = array()) {
  return perfectmoney_parse_csv(perfectmoney_query('historycsv', 'GET', array_merge($options, array(
    'AccountID'  => $account_id,
    'PassPhrase' => $password,
  ))));
}

/**
 * Verifies the validity of an intended Perfect Money payment.
 *
 * @see https://perfectmoney.com/documents/perfectmoney-api.doc#2.2
 */
function perfectmoney_verify_payment($account_id, $password, $payer, $payee, $amount, $memo = '', array $options = array()) {
  $result = perfectmoney_parse_fields(perfectmoney_query('verify', 'POST', array_merge($options, array(
    'AccountID'     => $account_id,
    'PassPhrase'    => $password,
    'Payer_Account' => $payer,
    'Payee_Account' => $payee,
    'Amount'        => $amount,
    'Memo'          => $memo,
  ))));
  return is_array($result);
}

/**
 * Performs a Perfect Money payment.
 *
 * @see https://perfectmoney.com/documents/perfectmoney-api.doc#2.1
 */
function perfectmoney_make_payment($account_id, $password, $payer, $payee, $amount, $memo = '', array $options = array()) {
  $result = perfectmoney_parse_fields(perfectmoney_query('confirm', 'POST', array_merge($options, array(
    'AccountID'     => $account_id,
    'PassPhrase'    => $password,
    'Payer_Account' => $payer,
    'Payee_Account' => $payee,
    'Amount'        => $amount,
    'Memo'          => $memo,
  ))));
  return is_array($result) ? $result['PAYMENT_BATCH_NUM'] : FALSE;
}

//////////////////////////////////////////////////////////////////////////////
// Perfect Money HTTP API

/**
 * Executes a Perfect Money API query using a HTTP GET request.
 */
function perfectmoney_query($op, $method = 'GET', array $args = array()) {
  $url = perfectmoney_build_query($op, $args);
  unset($GLOBALS['perfectmoney_error']);
  $result = @drupal_http_request($url, array(), $method, NULL, 1);
  if (isset($result->error)) {
    $GLOBALS['perfectmoney_error'] = $result->error;
  }
  return !isset($result->error) ? $result->data : FALSE;
}

/**
 * Builds a query URL for use with the HTTP GET API.
 */
function perfectmoney_build_query($method, array $args = array()) {
  $url = array();
  foreach ($args as $key => $value) {
    $value = (string)$value;
    // Special handling for the password/passphrase:
    $url[] = urlencode($key) . '=' . ($key == 'PassPhrase' ?
      perfectmoney_encode_password($value) : rawurlencode($value));
  }
  return PERFECTMONEY_API_URL . "$method.asp?" . implode('&', $url);
}

/**
 * Returns a password string with every character URL encoded.
 *
 * While API passwords are always transmitted as part of the HTTP request
 * over a secure SSL channel, we need to try and ensure that cleartext
 * passwords don't end up in PHP error messages or server logs in case an
 * error occurs.
 */
function perfectmoney_encode_password($input) {
  $output = '';
  for ($i = 0; $i < strlen($input); $i++) {
    $output .= '%' . sprintf('%2x', ord($input[$i]));
  }
  return $output;
}

/**
 * Returns the error, if any, from the previous API response.
 */
function perfectmoney_get_error() {
  return isset($GLOBALS['perfectmoney_error']) ? $GLOBALS['perfectmoney_error'] : FALSE;
}

/**
 * Parses API responses for the hidden HTML fields containing the actual
 * returned information.
 *
 * @see https://perfectmoney.com/documents/perfectmoney-api.doc
 */
function perfectmoney_parse_fields($input) {
  $fields = array();
  if (preg_match_all("/<input name='(.*)' type='hidden' value='(.*)'>/", $input, $results, PREG_SET_ORDER)) {
    foreach ($results as $result) {
      $fields[$result[1]] = $result[2];
    }
  }
  if (isset($fields['ERROR'])) {
    $GLOBALS['perfectmoney_error'] = $fields['ERROR'];
  }
  return isset($fields['ERROR']) ? $fields['ERROR'] : $fields;
}

/**
 * Parses API responses in CSV format.
 *
 * @see https://perfectmoney.com/documents/perfectmoney-api.doc
 */
function perfectmoney_parse_csv($input) {
  $lines = explode("\n", $input);
  if (count($lines) < 1 || preg_match('/^Error/', $input)) {
    return $input;
  }

  $fields = explode(',', array_shift($lines));
  $output = array();
  foreach ($lines as $line) {
    if ($line == 'No Records Found.') {
      return $output;
    }

    if (strlen(trim($line)) > 0) {
      $row = array();
      foreach (explode(',', $line, count($fields)) as $i => $field) {
        $row[$fields[$i]] = $field;
      }
      $output[] = $row;
    }
  }
  return $output;
}
