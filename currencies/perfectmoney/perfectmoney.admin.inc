<?php

//////////////////////////////////////////////////////////////////////////////
// Module settings

function perfectmoney_admin_settings() {
  module_load_include('inc', 'perfectmoney', 'perfectmoney.api');
  module_load_include('inc', 'perfectmoney', 'perfectmoney.sci');

  $form = array();

  // Payment settings
  perfectmoney_payment_settings($form, FALSE);

  // Exchange rates
  $form['rates'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Exchange rates'),
    '#collapsible'   => TRUE,
    '#collapsed'     => TRUE,
    '#description'   => perfectmoney_help('admin/settings/digitalcurrency/perfectmoney#rates'),
  );
  $form['rates']['perfectmoney_cron_rates'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Fetch the latest Perfect Money exchange rates on cron runs'),
    '#default_value' => variable_get('perfectmoney_cron_rates', 1),
  );
  $form['rates']['table'] = array('#value' => perfectmoney_admin_rates_table());

  return system_settings_form($form);
}

//////////////////////////////////////////////////////////////////////////////
// Exchange rates table

function perfectmoney_admin_rates_table() {
  $head = array(t('Currency 1'), t('Currency 2'), t('Currency 1/Currency 2'));
  $rows = array();

  // Construct the exchange rates table:
  foreach (perfectmoney_get_rates(NULL) as $unit1 => $units) {
    foreach ($units as $unit2 => $rate) {
      $rows[] = array($unit1, $unit2, $rate);
    }
  }

  if (empty($rows)) {
    $rows[] = array(array('data' => t('No rates available. Make sure that the data source URL is correct.'), 'colspan' => 3));
  }

  return theme('table', $head, $rows, array('class' => 'currency rates perfectmoney'));
}

//////////////////////////////////////////////////////////////////////////////
// Payment settings

function perfectmoney_payment_settings(&$form = array(), $extended = TRUE) {
  if ($extended) {
    $form['perfectmoney']['perfectmoney_payment_unit'] = array(
      '#type'          => 'select',
      '#title'         => t('Payment unit'),
      '#description'   => t('Transactions can at the moment only be processed in one of the listed currencies.'),
      '#options'       => drupal_map_assoc(array('USD', 'EUR', 'XAU')),
      '#default_value' => variable_get('perfectmoney_payment_unit', variable_get('uc_currency_code', 'USD')),
    );
  }
  perfectmoney_payment_settings_sci($form);
  perfectmoney_payment_settings_api($form);
  return $form;
}

function perfectmoney_payment_settings_sci(&$form = array()) {
  // SCI settings
  $form['perfectmoney_sci'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('SCI settings'),
    '#description'   => t('Settings for the Perfect Money <a href="http://www.perfectmoney.com/sample-api.html" target="_blank">Shopping Cart Interface (SCI)</a>.'),
    '#collapsible'   => TRUE,
    '#collapsed'     => TRUE,
  );
  $form['perfectmoney_sci']['perfectmoney_sci_payee_name'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Merchant name'),
    '#description'   => t('Enter a name that you wish to have displayed as the payee on the Perfect Money payment form. This would typically be the name of your website or company.'),
    '#default_value' => variable_get('perfectmoney_sci_payee_name', variable_get('uc_store_name', variable_get('site_name', 'Drupal'))),
    '#maxlength'     => 255,
    '#size'          => 30,
  );
  $form['perfectmoney_sci']['perfectmoney_sci_payee_account_usd'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Account for USD payments'),
    '#description'   => t('Enter the account identifier of a Perfect Money account that will be used for receiving payments denominated in U.S. dollars (USD).'),
    '#default_value' => variable_get('perfectmoney_sci_payee_account_usd', ''),
    '#maxlength'     => 10,
    '#size'          => 15,
  );
  $form['perfectmoney_sci']['perfectmoney_sci_payee_account_eur'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Account for EUR payments'),
    '#description'   => t('Enter the account identifier of a Perfect Money account that will be used for receiving payments denominated in euros (EUR).'),
    '#default_value' => variable_get('perfectmoney_sci_payee_account_eur', ''),
    '#maxlength'     => 10,
    '#size'          => 15,
  );
  $form['perfectmoney_sci']['perfectmoney_sci_payee_account_xau'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Account for XAU payments'),
    '#description'   => t('Enter the account identifier of a Perfect Money account that will be used for receiving payments denominated in troy ounces of gold (XAU).'),
    '#default_value' => variable_get('perfectmoney_sci_payee_account_xau', ''),
    '#maxlength'     => 10,
    '#size'          => 15,
  );
  return $form;
}

function perfectmoney_payment_settings_api(&$form = array()) {
  // API credentials
  $form['perfectmoney_api'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('API credentials'),
    '#description'   => t('Settings for the Perfect Money <a href="http://www.perfectmoney.com/sample-api.html" target="_blank">Application Programming Interface (API)</a>. In order to make use of any Perfect Money API functionality, you must first authorize this web server to access your Perfect Money account. To do so, log in to your Perfect Money account and in the account security settings enable API access, entering your web server\'s IP address (%ip) as the IP mask that will be allowed API access. <em><strong>Note:</strong> you do <strong>not</strong> need to provide API credentials in order to accept SCI payments using Ubercart.</em>', array('%ip' => $_SERVER['SERVER_ADDR'])),
    '#collapsible'   => TRUE,
    '#collapsed'     => TRUE,
  );
  $form['perfectmoney_api']['perfectmoney_api_member_id'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Perfect Money API member ID'),
    '#description'   => t('Enter your Member ID, a ten-digit string of the form U1234567. You should have received your Member ID by e-mail when registering your Perfect Money account.'),
    '#default_value' => variable_get('perfectmoney_api_member_id', ''),
    '#maxlength'     => 255,
    '#size'          => 30,
  );
  $form['perfectmoney_api']['perfectmoney_api_password'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Perfect Money API password'),
    '#description'   => t('Enter your Perfect Money password or passphrase. <em><strong>Note:</strong> you should use <strong>caution</strong> in storing your password here unless you have taken measures to secure the web server. For instance, it is probably a particularly bad idea to use API access from a shared web host.</em>'),
    '#default_value' => variable_get('perfectmoney_api_member_id', ''),
    '#maxlength'     => 255,
    '#size'          => 30,
  );
  return $form;
}
