<?php

//////////////////////////////////////////////////////////////////////////////
// Module settings

function pecunix_admin_settings() {
  module_load_include('inc', 'pecunix', 'pecunix.pri');
  $form = array();

  // Payment settings
  pecunix_payment_settings($form, FALSE);

  return system_settings_form($form);
}

//////////////////////////////////////////////////////////////////////////////
// Payment settings

function pecunix_payment_settings(&$form = array(), $extended = TRUE) {
  if ($extended) {
    $form['pecunix']['pecunix_payment_unit'] = array(
      '#type'          => 'select',
      '#title'         => t('Payment unit'),
      '#description'   => t('Transactions can at the moment only be processed in one of the listed currencies.'),
      '#options'       => pecunix_digitalcurrency_units(),
      '#default_value' => variable_get('pecunix_payment_unit', variable_get('uc_currency_code', 'USD')),
    );
  }
  pecunix_payment_settings_pri($form);
  return $form;
}

function pecunix_payment_settings_pri(&$form = array()) {
  // PRI settings
  $form['pecunix_pri'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('PRI settings'),
    '#description'   => t('Settings for the Pecunix <a href="http://info.pecunix.com/pecunix_pri.htm" target="_blank">Payment Receipt Interface (PRI)</a>.'),
    '#collapsible'   => TRUE,
    '#collapsed'     => TRUE,
  );
  $form['pecunix_pri']['pecunix_pri_payee_account'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Account for payments'),
    '#description'   => t('Enter the e-mail address of a Pecunix account that will be used for receiving payments. Example: <tt>payments@example.org</tt>'),
    '#default_value' => variable_get('pecunix_pri_payee_account', ''),
    '#size'          => 30,
  );
  $form['pecunix_pri']['pecunix_pri_who_pays_fees'] = array(
    '#type'          => 'select',
    '#title'         => t('Transaction fees paid by'),
    '#description'   => t('Select whether the payer (sender) or the payee (recipient) pays the <a href="http://pecunix.com/money.refined...ind.feestructure" target="_blank">transaction fees</a> charged by Pecunix.'),
    '#options'       => array('' => 'Default (recipient pays)', 'PAYER' => 'Payer (sender pays)', 'PAYEE' => 'Payee (recipient pays)', 'BOTH' => 'Both (both pay 50/50)'),
    '#default_value' => variable_get('pecunix_pri_who_pays_fees', ''),
  );
  $form['pecunix_pri']['pecunix_shared_secret'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Shared secret'),
    '#description'   => t('Enter a shared secret (8 to 50 characters) for use in the payment authentication hash. <strong>Note:</strong> you need to <a href="http://info.pecunix.com/pecunix_pri.htm#_Toc24400145" target="_blank">set up the Pecunix account</a> accordingly.'),
    '#default_value' => variable_get('pecunix_shared_secret', ''),
    '#maxlength'     => 50,
    '#size'          => 30,
  );
  return $form;
}
