<?php

//////////////////////////////////////////////////////////////////////////////
// Pecunix PRI settings

define('PECUNIX_PRI_URL', 'https://pri.pecunix.com/money.refined');

//////////////////////////////////////////////////////////////////////////////
// Ubertcart API hooks

function pecunix_payment_unit($default = 'USD') {
  $unit = variable_get('pecunix_payment_unit', variable_get('uc_currency_code', $default));
  return ($unit == 'XAU') ? 'OAU' : $unit; // Pecunix uses non-standard 'OAU' for troy ounces
}

function pecunix_payment_data($order) {
  //$order->order_total = 0.0001; // DEBUG

  return array(
    // Fields required by Pecunix
    'PAYEE_ACCOUNT'  => variable_get('pecunix_pri_payee_account', ''),
    'PAYMENT_UNITS'  => pecunix_payment_unit(),
    'PAYMENT_AMOUNT' => uc_currency_format($order->order_total, FALSE, FALSE, '.'),
    // Optional fields
    'WHO_PAYS_FEES'  => variable_get('pecunix_pri_who_pays_fees', ''),

    'STATUS_URL'     => url('cart/pecunix/status/' . uc_cart_get_id(), array('absolute' => TRUE)),
    'STATUS_TYPE'    => 'FORM',
    'PAYMENT_URL'    => url('cart/pecunix/finish/' . uc_cart_get_id(), array('absolute' => TRUE)),
    'NOPAYMENT_URL'  => url('cart/pecunix/cancel/' . uc_cart_get_id(), array('absolute' => TRUE)),
    'PAYMENT_ID'     => $order->order_id,
    'SUGGESTED_MEMO' => '',
  );
}

function pecunix_payment_charge($order_id, $amount, $data) {
  $order = uc_order_load($order_id); // TODO
}

function pecunix_payment_callback($op, $cart_id = NULL) {
  if (!($order = uc_order_load($order_id = $_POST[/*TODO*/'']))) {
    return drupal_not_found();
  }

  switch ($op) {
    case 'status':
      // TODO
      break;

    case 'finish':
      $output = uc_cart_complete_sale($order, variable_get('uc_new_customer_login', FALSE));
      return ($path = variable_get('uc_cart_checkout_complete_page', '')) ? drupal_goto($path) : $output;

    default:
    case 'cancel':
      uc_order_comment_save($order->order_id, 0, t('Pecunix payment canceled by customer.'), 'admin');
      return drupal_goto('cart/checkout'); // return to the checkout screen
  }
}

function pecunix_payment_verify($data, $secret) {
  // TODO
}
