<?php

//////////////////////////////////////////////////////////////////////////////
// Module settings

define('LIBERTYRESERVE_URL', 'http://www.libertyreserve.com/');

//////////////////////////////////////////////////////////////////////////////
// Core API hooks

/**
 * Implementation of hook_help().
 */
function libertyreserve_help($path) {
  switch ($path) {
    case 'admin/settings/digitalcurrency/libertyreserve':
      return '<p>' . theme('digitalcurrency_button', 'libertyreserve', 'Liberty Reserve', array('align' => 'right', 'style' => 'margin: 0px 0px 10px 10px;')) .
        t('<a href="@link" target="_blank">Liberty Reserve</a> is an account-based payment system for storing value and transferring payments denominated in USD, EUR, and gold grams (GAU). Payments are irrevocable, confidential, and, at the payer\'s option, anonymous. Liberty Reserve provides both shopping cart integration (SCI) and programmatic account access (API).', array('@link' => LIBERTYRESERVE_URL)) . '</p>';
  }
}

/**
 * Implementation of hook_menu().
 */
function libertyreserve_menu() {
  return array(
    // Administer >> Site configuration >> Digital currency >> Liberty Reserve
    'admin/settings/digitalcurrency/libertyreserve' => array(
      'title' => 'Liberty Reserve',
      'description' => 'Settings for the Liberty Reserve digital currency module.',
      'access arguments' => array('administer site configuration'),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('libertyreserve_admin_settings'),
      'file' => 'libertyreserve.admin.inc',
    ),
    // Ubercart payment processing callback
    'cart/libertyreserve/%' => array(
      'type' => MENU_CALLBACK,
      'access callback' => TRUE,
      'page callback' => 'libertyreserve_payment_callback',
      'page arguments' => array(2),
      'file' => 'libertyreserve.sci.inc',
    ),
  );
}

//////////////////////////////////////////////////////////////////////////////
// Digital Currency API hooks

/**
 * Implementation of hook_digitalcurrency().
 */
function libertyreserve_digitalcurrency() {
  return array('api' => TRUE, 'sci' => TRUE, 'button' => 'libertyreserve.gif');
}

//////////////////////////////////////////////////////////////////////////////
// Ubertcart API hooks

/**
 * Implementation of hook_payment_gateway().
 */
function libertyreserve_payment_gateway() {
  module_load_include('inc', 'libertyreserve', 'libertyreserve.admin');
  module_load_include('inc', 'libertyreserve', 'libertyreserve.sci');

  return array(
    'libertyreserve' => array(
      'id'              => 'libertyreserve',
      'title'           => t('Liberty Reserve'),
      'description'     => t('Process digital currency payments through Liberty Reserve.'),
      'settings'        => 'libertyreserve_payment_settings',
      'digitalcurrency' => 'libertyreserve_payment_charge',
      'submit_data'     => 'libertyreserve_payment_data',
      'submit_action'   => LIBERTYRESERVE_SCI_URL,
    ),
  );
}
