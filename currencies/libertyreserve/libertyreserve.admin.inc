<?php

//////////////////////////////////////////////////////////////////////////////
// Module settings

function libertyreserve_admin_settings() {
  module_load_include('inc', 'libertyreserve', 'libertyreserve.api');
  module_load_include('inc', 'libertyreserve', 'libertyreserve.sci');

  $form = array();

  // Payment settings
  libertyreserve_payment_settings($form, FALSE);

  return system_settings_form($form);
}

//////////////////////////////////////////////////////////////////////////////
// Payment settings

function libertyreserve_payment_settings(&$form = array(), $extended = TRUE) {
  if ($extended) {
    $form['libertyreserve']['libertyreserve_payment_unit'] = array(
      '#type'          => 'select',
      '#title'         => t('Payment unit'),
      '#description'   => t('Transactions can at the moment only be processed in one of the listed currencies.'),
      '#options'       => drupal_map_assoc(array('USD', 'EUR'/*, 'GAU'*/)),
      '#default_value' => variable_get('libertyreserve_payment_unit', variable_get('uc_currency_code', 'USD')),
    );
  }
  libertyreserve_payment_settings_sci($form);
  //libertyreserve_payment_settings_api($form); // TODO
  return $form;
}

function libertyreserve_payment_settings_sci(&$form = array()) {
  // SCI settings
  $form['libertyreserve_sci'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('SCI settings'),
    '#description'   => t('Settings for the Liberty Reserve <a href="http://www.libertyreserve.com/en/help/merchants/sci/" target="_blank">Shopping Cart Interface (SCI)</a>.'),
    '#collapsible'   => TRUE,
    '#collapsed'     => TRUE,
  );
  $form['libertyreserve_sci']['libertyreserve_sci_lr_acc'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Account for payments'),
    '#description'   => t('Enter the account identifier of a Liberty Reserve account that will be used for receiving payments. Examples: <tt>U1234567</tt>, <tt>X3214567</tt>'),
    '#default_value' => variable_get('libertyreserve_sci_lr_acc', ''),
    '#maxlength'     => 10,
    '#size'          => 15,
  );
  // TODO
  return $form;
}

function libertyreserve_payment_settings_api(&$form = array()) {
  // API credentials
  $form['libertyreserve_api'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('API credentials'),
    '#description'   => t('Settings for the Liberty Reserve <a href="http://www.libertyreserve.com/en/help/programmatic-access/api/" target="_blank">Application Programming Interface (API)</a>.'),
    '#collapsible'   => TRUE,
    '#collapsed'     => TRUE,
  );
  // TODO
  return $form;
}
