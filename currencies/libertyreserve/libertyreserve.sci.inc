<?php

//////////////////////////////////////////////////////////////////////////////
// Liberty Reserve SCI settings

define('LIBERTYRESERVE_SCI_URL', 'https://sci.libertyreserve.com/');

//////////////////////////////////////////////////////////////////////////////
// Ubertcart API hooks

function libertyreserve_payment_data($order) {
  //$order->order_total = 0.01; // DEBUG

  return array(
    'lr_acc'                => variable_get('libertyreserve_sci_lr_acc', ''),
    //'lr_store'            => variable_get('', ''), // TODO: advanced mode
    'lr_amnt'               => uc_currency_format($order->order_total, FALSE, FALSE, '.'),
    'lr_currency'           => 'LR' . variable_get('libertyreserve_payment_unit', variable_get('uc_currency_code', 'USD')),
    'lr_comments'           => '',
    'lr_merchant_ref'       => $order->order_id,
    'lr_success_url'        => url('cart/libertyreserve/finish/' . uc_cart_get_id(), array('absolute' => TRUE)),
    'lr_success_url_method' => 'POST',
    'lr_fail_url'           => url('cart/libertyreserve/cancel/' . uc_cart_get_id(), array('absolute' => TRUE)),
    'lr_fail_url_method'    => 'POST',
    'lr_status_url'         => url('cart/libertyreserve/status/' . uc_cart_get_id(), array('absolute' => TRUE)),
    'lr_status_url_method'  => 'POST',
  );
}

function libertyreserve_payment_charge($order_id, $amount, $data) {
  $order = uc_order_load($order_id); // TODO
}

function libertyreserve_payment_callback($op, $cart_id = NULL) {
  if (!($order = uc_order_load($order_id = $_POST['lr_merchant_ref']))) {
    return drupal_not_found();
  }

  switch ($op) {
    case 'status': // TODO
      if (libertyreserve_payment_verify($_POST, variable_get('libertyreserve_payment_unit', ''))) {
        watchdog('digitalcurrency', 'Receiving payment notification for order #@order.', array('@order' => $order_id), WATCHDOG_NOTICE, l(t('view'), 'admin/store/orders/' . $order->order_id . '/payments'));
        $comment = t('Liberty Reserve @unit payment, transaction number #@id.', array('@unit' => $_POST['lr_currency'], '@id' => $_POST['lr_transfer']));
        uc_payment_enter($order->order_id, 'digitalcurrency', $_POST['lr_amnt'], 0, $_POST, $comment);
      }
      else {
        watchdog('digitalcurrency', 'Received invalid payment notification for order #@order.', array('@order' => $order_id), WATCHDOG_ALERT, l(t('view'), 'admin/store/orders/' . $order->order_id . '/payments'));
      }
      break;

    case 'finish':
      $output = uc_cart_complete_sale($order, variable_get('uc_new_customer_login', FALSE));
      return ($path = variable_get('uc_cart_checkout_complete_page', '')) ? drupal_goto($path) : $output;

    default:
    case 'cancel':
      uc_order_comment_save($order->order_id, 0, t('Liberty Reserve payment canceled by customer.'), 'admin');
      return drupal_goto('cart/checkout'); // return to the checkout screen
  }
}

/**
 * @see http://www.libertyreserve.com/en/help/merchants/sci/#payments_auth
 */
function libertyreserve_payment_verify($data, $secret) {
  if (empty($data['lr_encrypted'])) { // in simple mode, no secure verification is possible
    return TRUE; // TODO: check a variable to see if we should allow unverified payments
  }
  else {
    if (!empty($secret) && function_exists('hash')) {
      $base = $data['lr_encrypted'];
      $data = array($data['lr_paidto'], $data['lr_paidby'], $data['lr_store'], $data['lr_amnt'], $data['lr_transfer'], $data['lr_currency'], $secret);
      $data = implode(':', $data);
      $hash = strtoupper(hash('sha256', $data)); // requires PHP 5.1.2+
      return $hash == $base;
    }
    return FALSE;
  }
}
