<?php

//////////////////////////////////////////////////////////////////////////////
// Settings form

function digitalcurrency_admin_settings() {
  $form = array('#tree' => TRUE);

  foreach (digitalcurrency_get_modules() as $module => $title) {
    $info = module_invoke($module, 'digitalcurrency');
    $info['weight'] = (int)db_result(db_query("SELECT weight FROM {system} WHERE type = 'module' AND name = '%s'", $module));

    $form[$module]['title']  = array('#value' => l($title, constant(strtoupper($module . '_URL')), array('attributes' => array('target' => '_blank')))); // FIXME
    $form[$module]['button'] = array('#value' => theme('digitalcurrency_button', $module, $title, array('align' => 'left', 'width' => 44, 'height' => 16, 'style' => 'margin: 0px 15px 0px 0px;')));
    $form[$module]['weight'] = array('#type' => 'weight', '#delta' => 10, '#default_value' => $info['weight']);
    $form[$module]['sci']    = array('#value' => !empty($info['sci']) ? t('yes') : t('no'));
    $form[$module]['api']    = array('#value' => !empty($info['api']) ? t('yes') : t('no'));
    $form[$module]['config'] = array('#value' => l(t('configure'), 'admin/settings/digitalcurrency/' . $module));
  }

  $form['#submit'][] = 'digitalcurrency_admin_settings_submit';
  return array_merge_recursive(system_settings_form($form), array('#theme' => 'digitalcurrency_admin_settings', 'buttons' => array('#weight' => 99)));
}

function theme_digitalcurrency_admin_settings($form) {
  $head = array(array('data' => t('Module'), 'colspan' => 1, 'width' => '50%'), t('SCI?'), t('API?'), t('Weight'), array('data' => t('Operations'), 'colspan' => '2'));

  $rows = array();
  foreach (element_children($form) as $key) {
    if (!isset($form[$key]['title'])) {
      continue;
    }

    $row = &$form[$key];
    $row['weight']['#attributes']['class'] = 'module-weight';
    $rows[] = array(
      'data' => array(
        drupal_render($row['button']) . drupal_render($row['title']),
        drupal_render($row['sci']),
        drupal_render($row['api']),
        drupal_render($row['weight']),
        drupal_render($row['config']),
      ),
      'class' => 'draggable',
    );
  }

  if (empty($rows)) {
    $rows[] = array(array('data' => t('No digital currency modules enabled.'), 'colspan' => 6));
  }

  $form['#value'] = theme('table', $head, $rows, array('id' => 'digitalcurrency-modules', 'class' => 'digitalcurrency modules'));
  drupal_add_tabledrag('digitalcurrency-modules', 'order', 'sibling', 'module-weight');

  return drupal_render($form);
}

function digitalcurrency_admin_settings_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#value'] == t('Reset to defaults')) {
    $modules = array_keys(digitalcurrency_get_modules());
    db_query("UPDATE {system} SET weight = 0 WHERE type = 'module' AND name IN (" . db_placeholders($modules) . ")"); // reset weights
    return;
  }

  $weighted = FALSE;
  $modules = array();
  foreach (digitalcurrency_get_modules() as $module => $title) {
    $modules[$weight = (int)$form_state['values'][$module]['weight']] = $module;
    $weighted = $weighted || !empty($weight);
    unset($form_state['values'][$module]); // don't save anything to the {variable} table
  }
  ksort($modules); // sort by weight, in ascending order

  if (!empty($modules) && $weighted) {
    $weight = (int)db_result(db_query("SELECT weight FROM {system} WHERE type = 'module' AND name = '%s'", 'digitalcurrency'));
    foreach ($modules as $module) {
      db_query("UPDATE {system} SET weight = %d WHERE type = 'module' AND name = '%s'", ++$weight, $module);
    }
  }

  unset($form_state['values']['buttons']); // don't save anything to the {variable} table
}

//////////////////////////////////////////////////////////////////////////////
// Buttons block

function digitalcurrency_admin_block_configure() {
  $form = array();
  foreach (digitalcurrency_get_modules() as $module => $title) {
    $form[$module] = array(
      '#type'          => 'fieldset',
      '#title'         => $title,
      '#tree'          => TRUE,
    );
    $form[$module]['button'] = array(
      '#type'          => 'checkbox',
      '#title'         => t('Display this button'),
      '#default_value' => variable_get('digitalcurrency_' . $module . '_button', 1),
    );
    $form[$module]['link'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Link URL'),
      '#default_value' => digitalcurrency_get_link($module),
    );
  }
  return $form;
}

function digitalcurrency_admin_block_submit($edit) {
  foreach (digitalcurrency_get_modules() as $module => $title) {
    foreach ($edit[$module] as $key => $value) {
      variable_set('digitalcurrency_' . $module . '_' . $key, $value);
    }
  }
}
